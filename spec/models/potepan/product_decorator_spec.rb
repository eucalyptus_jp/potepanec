require 'rails_helper'

RSpec.describe Potepan::ProductDecorator, type: :model do
  let(:taxon_1) { create(:taxon) }
  let(:taxon_2) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon_1, taxon_2]) }
  let(:related_products) { create_list(:product, 10, taxons: [taxon_1]) }
  let(:perfect_match_product) { create(:product, taxons: [taxon_1, taxon_2]) }
  let(:unrelated_product) { create(:product) }

  before { related_products << perfect_match_product }

  describe "Productモデル related_productsのテスト" do
    subject { product.related_products }

    it "同じカテゴリーの商品を全て含む" do
      is_expected.to match_array related_products
    end

    it "異なるカテゴリーの商品は含めない" do
      is_expected.not_to include unrelated_product
    end

    it "詳細が表示されていてる商品は含めない" do
      is_expected.not_to include product
    end

    it "taxon_idsが一致する商品を配列の前に移動させる" do
      expect(product.related_products.first).to eq perfect_match_product
    end
  end
end
