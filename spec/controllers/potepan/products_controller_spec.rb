require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "Productsコントローラ showのテスト" do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }

    before { get :show, params: { id: product.id } }

    it "正常にレスポンスを返す" do
      expect(response).to be_successful
    end

    it "showテンプレートを表示する" do
      expect(response).to render_template :show
    end

    it "productを取得する" do
      expect(assigns(:product)).to eq product
    end

    it "related_productsを取得する" do
      expect(assigns(:related_products)).to match_array related_products
    end

    context "関連商品が4個であるとき" do
      it "related_productsは4個である" do
        expect(assigns(:related_products).size).to eq 4
      end
    end

    context "関連商品が5個以上あるとき" do
      let!(:related_products) { create_list(:product, 6, taxons: [taxon]) }

      it "related_productsは4個である" do
        expect(assigns(:related_products).size).to eq 4
      end
    end

    context "関連商品が4個未満であるとき" do
      let!(:related_products) { create_list(:product, 2, taxons: [taxon]) }

      it "related_productsはその数だけある" do
        expect(assigns(:related_products).size).to eq 2
      end
    end

    it "related_productsの順番はランダムになっている" do
      expect(assigns(:related_products)).not_to match related_products
    end
  end
end
