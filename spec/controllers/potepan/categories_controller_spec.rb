require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "Categoriesコントローラ showのテスト" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon_root) { taxonomy.root }
    let(:taxon) { create(:taxon, taxonomy: taxonomy, parent: taxon_root) }
    let(:product) { create(:product, taxons: [taxon]) }

    before { get :show, params: { id: taxon.id } }

    it "正常にレスポンスを返す" do
      expect(response).to be_successful
    end

    it "showテンプレートを表示する" do
      expect(response).to render_template :show
    end

    it "taxonを取得する" do
      expect(assigns(:taxon)).to eq taxon
    end

    it "taxonomiesを取得する" do
      expect(assigns(:taxonomies)).to contain_exactly taxonomy
    end

    it "productsを取得する" do
      expect(assigns(:products)).to contain_exactly product
    end
  end
end
