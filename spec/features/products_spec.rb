require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let(:taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:related_product) { create(:product, taxons: [taxon]) }

  before { visit potepan_product_path(product.id) }

  scenario "商品詳細ページを閲覧する" do
    expect(page).to have_title product.name
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    expect(page).to have_content product.description
    within(".navbar") do
      expect(page).to have_link 'Home'
    end
    within(".lightSection") do
      expect(page).to have_link 'Home'
    end
    within(".productsContent") do
      expect(page).to have_content related_product.name
      expect(page).to have_content related_product.display_price
    end
  end

  scenario "HOMEリンク(NAVBAR)の挙動確認" do
    find(".navbar").click_link "Home"
    expect(current_path).to eq potepan_index_path
  end

  scenario "HOMEリンク(LIGHT SECTION)の挙動確認" do
    find(".lightSection").click_link "Home"
    expect(current_path).to eq potepan_index_path
  end

  scenario "「一覧ページへ戻る」リンクの挙動確認" do
    find(".singleProduct").click_link "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(product.taxons.first.id)
  end

  scenario "関連商品リンクの挙動確認" do
    find(".productsContent").click_link related_product.name
    expect(current_path).to eq potepan_product_path(related_product.id)
  end
end
