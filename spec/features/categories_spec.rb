require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon_root) { taxonomy.root }
  let(:taxon) { create(:taxon, taxonomy: taxonomy, parent: taxon_root) }
  let!(:other_taxon) { create(:taxon, name: 'other_taxon', taxonomy: taxonomy, parent: taxon_root) }
  let!(:product) { create(:product, taxons: [taxon]) }

  before { visit potepan_category_path(taxon.id) }

  scenario "カテゴリーページを閲覧する" do
    expect(page).to have_title taxon.name
    within ".side-nav" do
      expect(page).to have_content taxonomy.name
      expect(page).to have_content 'other_taxon'
    end
    within ".test_productbox" do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
    end
  end

  scenario "taxonリンクの挙動確認" do
    find(".side-nav").click_link other_taxon.name
    expect(current_path).to eq potepan_category_path(other_taxon.id)
  end

  scenario "productリンクの挙動確認" do
    find(".test_productbox").click_link product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end
end
