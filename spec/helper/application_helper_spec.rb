require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "Applicationヘルパー full_titleのテスト" do
    subject { full_title(page_title) }

    context "page_titleがnilのとき" do
      let(:page_title) { nil }

      it { is_expected.to eq 'BIGBAG Store' }
    end

    context "page_titleが空白のとき" do
      let(:page_title) { '' }

      it { is_expected.to eq 'BIGBAG Store' }
    end

    context "page_titleがexampleのとき" do
      let(:page_title) { 'example' }

      it { is_expected.to eq 'example - BIGBAG Store' }
    end
  end
end
