module Potepan::ProductDecorator
  def related_products
    Spree::Product.
      in_taxons(taxons).
      includes(master: [:default_price, :images]).
      distinct.
      where.not(id: id).
      partition { |product| product.taxon_ids == taxon_ids }.
      flatten
  end

  Spree::Product.prepend self
end
