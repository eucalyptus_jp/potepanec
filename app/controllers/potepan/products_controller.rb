class Potepan::ProductsController < ApplicationController
  MAX_COUNT_FOR_SQL = 12
  MAX_COUNT_OF_DISPLAY = 4
  def show
    @product = Spree::Product.friendly.find(params[:id])
    @related_products =
      @product.related_products.
        first(MAX_COUNT_FOR_SQL).
        sample(MAX_COUNT_OF_DISPLAY)
  end
end
